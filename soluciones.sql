﻿USE concursos;

/*
  1.1 Indicar el nombre de los concursantes de Burgos
*/

  SELECT 
    DISTINCT c.nombre 
  FROM
    concursantes  c 
  WHERE 
    c.provincia = 'Burgos';

  -- Vamos a dejar el LIKE por si tenemos varios Burgos entonces pondríamos LIKE '%Burgos%', el like es muy lento
/* 
  1.2 Indicar cuántos concursantes hay de Burgos
  */

    -- consultas de fila única
  SELECT 
    COUNT(*) 
  FROM 
    concursantes c 
  WHERE 
    c.provincia = 'Burgos';

/*
  1.3 Indicar cuantos concursantes hay de cada poblacion
  */

  SELECT
    c.poblacion, COUNT(*) 
  FROM 
    concursantes c 
  GROUP BY c.poblacion;

/*
  1.4 Indicar las poblaciones que tienen concursantes de menos de 90Kg
  */
  SELECT DISTINCT 
    c.poblacion 
  FROM 
    concursantes c 
  WHERE c.peso <90;

/*
  1.5 Indicar las poblaciones que tienen más de 1 concursante 
  */

  -- solucion 1 con having 
  SELECT 
    c.poblacion
  FROM 
    concursantes c 
  GROUP BY 
    c.poblacion 
  HAVING 
    COUNT(*)>1;

  -- el group by es bastante lento, aun asi quita repetidos. Se puede evitar el DISTINCT, no hace falta ponerlo

  -- en sgbd diferentes de mysql puede no existir HAVING, habría que hacerlo con where despues de agrupar

  -- solucion 2 sin having
  SELECT c1.poblacion FROM (
    SELECT c.poblacion, COUNT(*) nConcursantes 
      FROM concursantes c 
      GROUP BY c.poblacion) c1
    WHERE c1.nConcursantes>1;

  -- en algebra tienes que hacer una seleccion porque no hay having


/*
  1.6 Indicar las poblaciones que tienen mas de 1 concursante de menos de 90Kg
  */
  -- un having es una selección después de los totales

  SELECT * FROM concursantes c WHERE c.peso <90;

  SELECT c1.poblacion FROM (
    SELECT * FROM concursantes c WHERE c.peso <90
    ) c1 
  GROUP BY c1.poblacion HAVING COUNT(*)>1;